Запускаем команду ansible-playbook -i ../terraform/inventory.ini main.yml, используя файл полученный в terraform
(перед первым запуском плейбка отключаем проверку fingerprint)
Описание плейбука:
 - запускаются роли для установки и настройки:
    - nodeexporter - на всех хостах, для мониторинга всех хостов
    - docker - на хостах service и infra - для запуска dockerimage. Loadbalancer пробрасывает трафик на эти два хоста
    - grafana - на хосте grafana 
    - prometheus - на хосте prometheus
 - добавляется пользователь ubuntu в группу docker
 - устанавливаются зависимости для работы с контейнеризованными приложениями: python3-pip, через пайп - docker, docker-compose 
 - копируются фаайлы на инстанс с правами права на файлы (704),  чтение для всех групп необходимо для файла index.html
 - запускается образ dockerhabaav/mywebnginx с тегом (Release), переданным через пайплайн на порту 80 
 - в запущенный докер контейнер копируется обновленный index.html, опция -a, указанная при копировании, сохраняет права на файл (чтобы наглядней продемонстрировать работу loadBalancer).  