#!/bin/bash

ipaddress=$(ip --brief address show eth0 | awk '{print $3}' | cut -d'/' -f1) 
sed -i "s/ip address/Internal ip address=$ipaddress/g" /tmp/index.html 