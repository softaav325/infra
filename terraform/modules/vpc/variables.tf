variable "region" {
  type    = string
  default = "ru-central1-a"
}

variable "cloud" {
  description = "Enter id cloud"
  type        = string
  default     = "b1g3r1df58jn3i647ut5"
}

variable "folder" {
  description = "Enter id folder "
  type        = string
}

variable "network_name" {
  description = "Enter network name"
  type        = string
}