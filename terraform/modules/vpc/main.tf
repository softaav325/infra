# ------------- Create resource network -----------------------
resource "yandex_vpc_network" "vpc-1" {
   name = var.network_name
}

# ------------- Create resource subnet -----------------------
resource "yandex_vpc_subnet" "subnet-1" {
  #zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc-1.id
  v4_cidr_blocks = ["10.10.10.0/24"]
}
