terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = "bucket/key.json"
  zone                     = var.region
  cloud_id                 = var.cloud
  folder_id                = var.folder
}