# ------------- Create a resource record -----------------------
resource "yandex_dns_recordset" "rs_dev_infra" {
  count = length(var.name_dns_zone)
  zone_id = var.yandex_dns_zone
  name = "${var.name_dns_zone[count.index]}"
  type    = "A"
  ttl     = 200
  data    = [var.ip_instance[count.index]]
}