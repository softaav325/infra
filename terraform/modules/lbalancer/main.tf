# ------------- Create static ip address for load balancer----------------
resource "yandex_vpc_address" "static-ip" {
  name                = "staticIp"
  #deletion_protection = true
  external_ipv4_address {
    zone_id = var.region
  }
}

# ------------- Create Target group-----------------------
resource "yandex_lb_target_group" "tggroup" {
  name      = var.name_tgroup

  dynamic "target" {
    for_each = var.target_group
    content {
      subnet_id = var.yandex_vpc_subnet  
      address   = target.value
    }
  }
}

# ------------- Create Load balancer with static ip-----------------------
resource "yandex_lb_network_load_balancer" "lb-1" {
  name                = var.name_lbalancer
  deletion_protection = false

  listener {
    name = "listener-1"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
      address    = yandex_vpc_address.static-ip.external_ipv4_address[0].address
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.tggroup.id

    healthcheck {
      name = "http"
      http_options {
        port = 80
      }
    }
  }
}