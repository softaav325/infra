#--------------------------Create security group-------------------------
resource "yandex_vpc_security_group" "secgroup" {
  name        = "securitygroup"
  description = "security group for vpc_network"
  network_id  = var.yandex_vpc_network
  dynamic "ingress" {
    for_each = var.ports
    content {
      protocol       = "TCP"
      description    = ""
      v4_cidr_blocks = ["0.0.0.0/0"]
      from_port      = ingress.value
      to_port        = ingress.value
    }
  }

  egress {
    protocol       = "ANY"
    description    = "The rule allows all outgoing traffic"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}
