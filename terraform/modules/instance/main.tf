#  ------------- Create instances -----------------
resource "yandex_compute_instance" "instance" {
  count = var.server_count
  name        = "${var.name}${count.index}"
  platform_id = "standard-v1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
    }
  }

  network_interface {
    subnet_id = var.yandex_vpc_subnet
    nat       = true
  }

  metadata = {
    ssh-keys  = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    user-data = "${file("start.sh")}"
  } 
}