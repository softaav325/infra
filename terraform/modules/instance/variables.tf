variable "region" {
  type    = string
  default = "ru-central1-a"
}

variable "cloud" {
  description = "Enter id cloud"
  type        = string
  default     = "b1g3r1df58jn3i647ut5"
}

variable "folder" {
  description = "Enter id folder "
  type        = string
}

variable "name" {
  type        = string
  description = "name instance"
}

variable "server_count" {
  description = "quntity instanse"
  type        = number
  default     = 0
}

variable "image_id" {
  description = "Enter id boot disk"
  type        = string
}

variable "yandex_vpc_subnet" {
  description = "Enter vpc subnet"
}