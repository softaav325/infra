[all:vars]
ansible_user=ubuntu 
ansible_ssh_private_key=~/.ssh/id_rsa.pub

[servers]
server_prod_1 ansible_host=${server_1} 
server_prod_2 ansible_host=${server_2} 

[monitoring]
server_3 ansible_host=${server_3} 
server_4 ansible_host=${server_4} 

[servers_promtail:children]
servers
 
[servers_service]
server_prod_2 ansible_host=${server_2} 
