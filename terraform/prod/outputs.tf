resource "local_file" "inventorydata" {
  filename = "inventory.ini"
  content = templatefile("inventory.ini.tpl", {
    server_1 = module.create_prod_instance.ext_ip[0]
    server_2 = local.ip[1]
    server_3 = local.ip[2]
    server_4 = local.ip[3]
  })
  provisioner "local-exec" {
    command = "echo $ipinfra >> /home/gitlab-runner/infra/ansible/roles/prometheus/vars/main.yml"
      environment = {
        ipinfra = "ipinfra: ${module.create_prod_instance.ext_ip[0]}"
    }
  }
}

resource "null_resource" "ipgrafana" {
  provisioner "local-exec" {
    command =  "echo $ipgrafana >> /home/gitlab-runner/infra/ansible/roles/grafana/vars/ipgrafana.yml"
      environment = {
        ipgrafana = "ipgrafana: \"${module.create_prod_instance.ext_ip[3]}\""
    }
  }
}