resource "local_file" "accountdata" {
  filename = "datakey.tpl"
  content = templatefile("datakey.pattern.tpl", {
    access = yandex_iam_service_account_static_access_key.sa-key.access_key
    secret = yandex_iam_service_account_static_access_key.sa-key.secret_key
  })
}
