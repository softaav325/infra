variable "region" {
  type    = string
  default = "ru-central1-a"
}

variable "cloud" {
  description = "Enter cloud"
  type        = string
  default     = "b1g3r1df58jn3i647ut5"
 }

variable "folder" {
  description = "Enter folder cloud"
  type        = string
  default     = "b1gegre7hk3k0rjp59ol"
}

variable "sabucket" {
  description = "Enter name service accout bucket"
  default     = "sabucket-prod"
  type        = string
}

variable "bucketname" {
  description = "Enter bucket name"
  default     = "bucket-prod"
  type        = string
}