terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

  backend "s3" {
    endpoints = {
      s3 = "https://storage.yandexcloud.net"
    }
    bucket                      = "bucket-prod"
    region                      = "ru-central1"
    key                         = "terraform.tfstate"
    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true # This option is required to describe backend for Terraform version 1.6.1 or higher.
    skip_s3_checksum            = true # This option is required to describe backend for Terraform version 1.6.3 or higher.
    skip_metadata_api_check     = true
    shared_credentials_files    = ["bucket/datakey.tpl"]
  }
}

provider "yandex" {
  service_account_key_file = "bucket/key.json"
  zone                     = var.region
  cloud_id                 = var.cloud
  folder_id                = var.folder
}