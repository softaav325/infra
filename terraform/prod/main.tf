# ------------- Create vpc PROD----------------------------
module "create_prod_vpc" {
  source = "../modules/vpc/"
  network_name = "Network-prod-vpc"
  folder = var.folder
}

# ------------- Create instances PROD-----------------------
module "create_prod_instance" {
  server_count = 4
  source = "../modules/instance/"
  folder = var.folder
  name = "vmprod-"
  image_id = var.image_id
  yandex_vpc_subnet = module.create_prod_vpc.vpc_subnet
}

# ------------- Create local vars ip adress for load alancer and dns ------------------
locals {
  ip = [module.create_load_balancer.ext_ip_lb, module.create_prod_instance.ext_ip[1], module.create_prod_instance.ext_ip[2], module.create_prod_instance.ext_ip[3]]
  ip_lb = [module.create_prod_instance.int_ip[0], module.create_prod_instance.int_ip[1]]
}

# ------------- Create security grup PROD-----------------------
module "create_prod_sec_group" {
  source = "../modules/secgroup/"
  region = var.region
  cloud = var.cloud
  folder = var.folder
  yandex_vpc_network = module.create_prod_vpc.vpc_id
  ports = [22, 80, 443]
}

# ------------- Create Load Balancer-----------------------
module "create_load_balancer" {
  source = "../modules/lbalancer/"
  region = var.region
  cloud = var.cloud
  folder = var.folder
  name_tgroup = "targetgroup"
  yandex_vpc_subnet = module.create_prod_vpc.vpc_subnet
  name_lbalancer = "loadbalancer"
  target_group = local.ip_lb
}

# ------------- Create DNS rec-----------------------
module "create_dns_rec" {
 source = "../modules/dnsrec/"
 region = var.region
 cloud = var.cloud
 folder = var.folder
 name_dns_zone = ["infra.${var.my_dns}", "service.${var.my_dns}", "monitoring.${var.my_dns}", "grafana.${var.my_dns}"]
 ip_instance = local.ip
 yandex_dns_zone = var.yandex_dns_zone_id
}