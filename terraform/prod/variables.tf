variable "region" {
  description = "Enter region"
}

variable "cloud" {
  description = "Enter id cloud"
}

variable "folder" {
  description = "Enter id folder "
}

variable "image_id" {
  description = "Enter boot image id for instance"
}

variable "my_dns" {
  description = "Enter DNS Zone"
}

variable "yandex_dns_zone_id" {
  description = "Enter DNS Zone id"
}
