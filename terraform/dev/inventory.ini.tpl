[all:vars]
ansible_user=ubuntu 
ansible_ssh_private_key=~/.ssh/id_rsa.pub

[servers]
server_dev_1 ansible_host=${server_1} 
server_dev_2 ansible_host=${server_2} 

[servers_service]
server_dev_1 ansible_host=${server_1} 