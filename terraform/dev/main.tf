# ------------------Make public DNS zone------------
resource "yandex_dns_zone" "myzone" {
  name        = "public-zone"
  description = "Description public zone"
  zone        = var.my_dns
  public      = true
  provisioner "local-exec" {
    command = "echo $ya >> ../prod/varprod.auto.tfvars"
      environment = {
        ya = "yandex_dns_zone_id = \"${yandex_dns_zone.myzone.id}\""
    }
  }
}

# ------------- Create vpc DEPLOY-----------------------
module "create_deploy_vpc" {
  source = "../modules/vpc/"
  network_name = "Network-deploy-vpc"
  folder = var.folder
}

# ------------- Create instances DEPLOY-----------------------
module "create_deploy_instance" {
  server_count = 2
  source = "../modules/instance/"
  folder = var.folder
  name = "vmdev-"
  image_id = var.image_id
  yandex_vpc_subnet = module.create_deploy_vpc.vpc_subnet
}

# ------------- Create local var ip instances-----------------------
locals {
  ip = module.create_deploy_instance.ext_ip
}

# ------------- Create DNS rec-----------------------
module "create_dns_rec" {
 source = "../modules/dnsrec/"
 region = var.region
 cloud = var.cloud
 folder = var.folder
 name_dns_zone = ["testinfra.${var.my_dns}", "testservice.${var.my_dns}"]
 ip_instance = local.ip
 yandex_dns_zone = yandex_dns_zone.myzone.id
}