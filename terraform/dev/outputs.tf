resource "local_file" "inventorydata" {
  filename = "inventory.ini"
  content = templatefile("inventory.ini.tpl", {
    server_1 = local.ip[0]
    server_2 = local.ip[1]
  })
}
