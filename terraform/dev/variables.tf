variable "region" {
   description = "Enter region"
}

variable "cloud" {
  description = "Enter id cloud"
}

variable "folder" {
  description = "Enter id folder, default test folder"
}

variable "my_dns" {
  description = "Enter DNS Zone"
}

variable "image_id" {
  description = "Enter boot image id for instance"
}
