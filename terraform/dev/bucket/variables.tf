variable "region" {
  type    = string
  default = "ru-central1-a"
}

variable "cloud" {
  description = "Enter cloud"
  type        = string
  default     = "b1g3r1df58jn3i647ut5"
 }

variable "folder" {
  description = "Enter folder cloud"
  type        = string
  default     = "b1gdcstnilk6pvdcje57"
}

variable "sabucket" {
  description = "Enter name service accout bucket"
  default     = "sabucketdev"
  type        = string
}

variable "bucketname" {
  description = "Enter bucket name"
  default     = "bucket1-dev"
  type        = string
}