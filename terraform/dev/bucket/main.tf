terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = "key.json"
  zone      = var.region 
  cloud_id  = var.cloud
  folder_id = var.folder
}

// Создание сервисного аккаунта
resource "yandex_iam_service_account" "sa" {
  name = var.sabucket
}

// Назначение роли сервисному аккаунту
resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
  folder_id = var.folder 
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

// Создание статического ключа доступа
resource "yandex_iam_service_account_static_access_key" "sa-key" {
  service_account_id = yandex_iam_service_account.sa.id
  description        = "static access key for object storage"
}

// Создание бакета с использованием ключа
resource "yandex_storage_bucket" "bucketdevinfra" {
  access_key            = yandex_iam_service_account_static_access_key.sa-key.access_key
  secret_key            = yandex_iam_service_account_static_access_key.sa-key.secret_key
  bucket                = var.bucketname
  #max_size              = 100000
  force_destroy = true
  default_storage_class = "standard"
  anonymous_access_flags {
    read        = true
    list        = false
    config_read = false
  }
}